
package AccountManagementSystem;

import java.util.Random;

public class SavingAccount extends Accounts implements BaseInterestRate{

    int lockerid, lockerpin;
    // Constructor
  public SavingAccount(String name, String aadhaar, String accType, int initialDep) {
        //super();
      this.name = name;
      this.aadhaar= aadhaar;
      this.accType=accType;
      this.initialDep=initialDep;
      this.roi = rateForSaving();
      this.accnumber = "1"+ AdhaarLast3Digit() + UniqueNumGen();
     // System.out.println("Generated Saving Account number : "+accnumber);
      this.lockerid = LockerId();
      this.lockerpin = LockerPin();
     // System.out.println("Locker ID generated : " + lockerid+"\nlocker pin generated : "+lockerpin);
      this.showDetails();
    }

    //To calculate Rate of interest for Saving Account
    public static double rateForSaving() {
       double ROIS = (baseRate - 0.5);
        return ROIS;
    }

    //To Generate Locker ID & Pin
        public int LockerId(){
        Random LockerID = new Random();
//        int LockerID3Digit = LockerID.nextInt(999);
            return LockerID.nextInt(999);
        }

    public int LockerPin(){
        Random LockerID = new Random();

//        int LockerPin4Digit = LockerID.nextInt(9999);
        return LockerID.nextInt(9999);
    }

    public void showDetails()
    {
        System.out.println("Customer Name : "+this.name);
        System.out.println("Customer Aadhar : "+this.aadhaar);
        System.out.println("Account Type : " + this.accType);
        System.out.println("Initial Deposit : " +this.initialDep);
        System.out.println("Rate of interest : "+this.roi);
        System.out.println("Account Number : " +this.accnumber);
        System.out.println("Locker ID generated : "+ this.lockerid);
        System.out.println("Locker PIN generated : "+ this.lockerpin);
    }

}

