
package AccountManagementSystem;

import java.util.Random;

public class CurrentAccount extends Accounts implements BaseInterestRate {

    String card = "";
    int pin ;
    // Constructor
    public CurrentAccount(String name, String aadhaar, String accType, int initialDep) {
        //super();
        this.name = name;
        this.aadhaar= aadhaar;
        this.accType=accType;
        this.initialDep=initialDep;
        this.roi = rateForCurrent();
        this.accnumber = "2"+ AdhaarLast3Digit() + UniqueNumGen();
       // System.out.println("Generated Current Account number : "+accnumber);
        this.card = ATMCard();
       // System.out.println("Card Number generated : "+ card);
        this.pin = ATMPin();
       // System.out.println("Card PIN generated : "+ pin);
        this.showDetails();
    }

    //To calculate Rate of interest for Current Account
    public static double rateForCurrent() {
        double ROIC = baseRate*0.2;
        return ROIC;
    }
    //To Generate ATM no and PIN for current account
    public String ATMCard(){
        Random DebitATM = new Random();
//        String DebitATM16Digit = String.format("5555-4444-3333-2222",DebitATM.nextInt(9999),DebitATM.nextInt(9999),DebitATM.nextInt(9999),DebitATM.nextInt(9999));
        String DebitATM16Digit = DebitATM.nextInt(9999) + "-" + DebitATM.nextInt(9999) + "-" + DebitATM.nextInt(9999) + "-" + DebitATM.nextInt(9999);
        return DebitATM16Digit;
        }
    public int ATMPin(){
        Random DebitATM = new Random();
        return DebitATM.nextInt(9999);

    }

    public void showDetails()
    {
        System.out.println("Customer Name : "+this.name);
        System.out.println("Customer Aadhar : "+this.aadhaar);
        System.out.println("Account Type : " + this.accType);
        System.out.println("Initial Deposit : " +this.initialDep);
        System.out.println("Rate of interest : "+this.roi);
        System.out.println("Account Number : " +this.accnumber);
        System.out.println("Card Number generated : "+ this.card);
        System.out.println("Card PIN generated : "+ this.pin);
    }
   }

